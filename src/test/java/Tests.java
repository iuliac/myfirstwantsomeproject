import ro.wantsome.Utils;
import org.junit.Test;
import ro.wantsome.Utils;

import java.io.IOException;
import java.util.*;

import static junit.framework.TestCase.assertTrue;
import static org.junit.Assert.assertEquals;

public class Tests {
    @Test
    public void testPalindromFinder() {

        Set<String> inputSet = new HashSet<>();
        inputSet.add("cojoc");
        Set<String> palindromes = Utils.findPalindromes(inputSet);
        assertTrue(palindromes.contains("cojoc"));
    }

    @Test
    public void testPalindromFinder_FindsMultiple() {
        Set<String> inputSet = new HashSet<>();
        inputSet.add("cojoc");
        inputSet.add("asa");
        inputSet.add("acasa");
        inputSet.add("penar");
        Set<String> palindromes = Utils.findPalindromes(inputSet);
        assertEquals(2, palindromes.size());

    }

    @Test
    public void testPalindromFinder_IsResilient() {

        Set<String> palindromes = Utils.findPalindromes(null);
        assertEquals(2, palindromes.size());

    }

    @Test
    public void testSortLetters() {
        String word = "abracadabra";
        String sortedWord = Utils.sortLetters(word);
        System.out.println(sortedWord);
        assertEquals("aaaaabbcdrr", sortedWord);
    }

    @Test
    public void testFindAnagramsForAGivenWord() throws IOException {

        List<String> allLines = new ArrayList<>();
        //  allLines = Utils.readAllLinesFromResourcesFile("dex.txt");
        Set<String> wordSet = Utils.removeDuplicates(allLines);
        String word = "anonim";
        Map<String, List<String>> anagrams = Utils.findAnagramsForAGivenWord(wordSet, word);

        for (Map.Entry<String, List<String>> entry : anagrams.entrySet()) {
            System.out.println("Key is: " + entry.getKey() + "  ,  Value is: " + entry.getValue());
        }
    }

    @Test
    public void testFindAnagramsForAllWords() throws IOException {
        List<String> allLines = new ArrayList<>();
        // allLines = Utils.readAllLinesFromResourcesFile("dex.txt");
        Set<String> wordSet = Utils.removeDuplicates(allLines);
        Map<String, List<String>> anagrams = Utils.findAnagramsForAllWords(wordSet);
        for (Map.Entry<String, List<String>> entry : anagrams.entrySet()) {
            System.out.println("Key is: " + entry.getKey() + "  ,  Value is: " + entry.getValue());
        }

    }

    @Test
    public void testRandomWordFinder() {
        Set<String> inputSet = new HashSet<>();
        inputSet.add("cojoc");
        inputSet.add("asa");
        inputSet.add("acasa");
        inputSet.add("penar");

        String randomWord = Utils.getRandomWordFromSet(inputSet, new Random(1L));
        System.out.println(randomWord);
        assertEquals("acasa", randomWord);

    }


}
