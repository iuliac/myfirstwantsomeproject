package ro.wantsome;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;

public class Utils {
    /**
     * Can read a given file from the resources folder
     *
     * @return list of all the lines in the file
     */


    public static Set<String> removeDuplicates(List<String> allLines) {
        Set<String> wordSet = new HashSet<>();
        for (String line : allLines) {
            wordSet.add(line);
        }
        return wordSet;
    }

    public static Set<String> findWordsThatContain(Set<String> wordSet, String word) {
        Set<String> result = new HashSet<>();
        for (String line : wordSet) {
            if (line.contains(word))
                result.add(line);
        }
        return result;
    }

    public static Set<String> findPalindromes(Set<String> wordList) {


        Set<String> result = new HashSet<>();
        if (wordList == null) {
            System.out.println("Warning: null parameter for findPalindromes()");
            return result;
        }
        for (String line : wordList) {

            StringBuilder reversedWord = new StringBuilder(line).reverse();
            if (line.equals(reversedWord.toString()))
                result.add(line);

        }
        return result;
    }

    public static String sortLetters(String word) {

        String result = "";
        char[] arrayOfLetters = word.toCharArray();
        Arrays.sort(arrayOfLetters);
        for (char letter : arrayOfLetters) {
            result += letter;
        }
        return result;
    }

    public static Map<String, List<String>> findAnagramsForAGivenWord(Set<String> wordSet, String word) {
        Map<String, List<String>> result = new HashMap<>();
        List<String> list = new ArrayList<>();
        if (wordSet.contains(word)) {
            for (String line : wordSet) {
                String sortedWord = sortLetters(word);
                String sortedLine = sortLetters(line);
                if (sortedLine.equals(sortedWord)) {
                    list.add(line);
                }
                if (list.size() >= 2) {
                    result.put(sortedWord, list);
                }
            }
        } else System.out.println("The word is not in the list.");

        return result;
    }

    public static Map<String, List<String>> findAnagramsForAllWords(Set<String> wordSet) {
        Map<String, List<String>> result = new HashMap<>();
        for (String word : wordSet) {
            String sortedWord = sortLetters(word);

            if (result.containsKey(sortedWord)) {
                result.get(sortedWord).add(word);
            } else {
                List<String> list = new ArrayList<>();
                list.add(word);
                result.put(sortedWord, list);
            }
        }
        result.entrySet().removeIf(entry -> entry.getValue().size() < 2);

        return result;
    }

    public static List<String> sortSet(Set<String> unsorted) {
        List<String> result = new ArrayList<>(unsorted);
        Collections.sort(result);

        return result;
    }

    public static String getRandomWord(Set<String> wordsSet) {

        Random random = new Random();
        return getRandomWordFromSet(wordsSet, random);
    }

    public static String getRandomWordFromSet(Set<String> wordsSet, Random random) {
        List<String> wordsList = new ArrayList<>(wordsSet);
        int index = random.nextInt(wordsList.size());
        return wordsList.get(index);
    }
}
