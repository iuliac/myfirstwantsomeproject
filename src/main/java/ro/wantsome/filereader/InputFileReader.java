package ro.wantsome.filereader;

import java.io.IOException;
import java.util.List;

public interface InputFileReader {

    List<String>readFile(String input) throws IOException;
}
