package ro.wantsome;

import ro.wantsome.dictionaryoperations.*;
import ro.wantsome.filereader.GlobalInputFileReader;
import ro.wantsome.filereader.InputFileReader;
import ro.wantsome.filereader.ResourceInputFileReader;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.sql.SQLOutput;
import java.util.*;

public class Main {

    public static void main(String[] args) throws IOException {

        List<String> allLines = new ArrayList<>();
        InputFileReader inputFileReader = new ResourceInputFileReader();
        allLines = inputFileReader.readFile("dex.txt");

       /* InputFileReader inputFileReader= new GlobalInputFileReader();
        allLines= inputFileReader.readFile("C:\\code\\iulia-project\\src\\main\\resources\\dex.txt");*/

        Set<String> wordsSet = Utils.removeDuplicates(allLines);

        BufferedReader in = new BufferedReader(new InputStreamReader((System.in)));

        while (true) {
            System.out.println("Menu:\n\t" +
                    "1. Search\n\t" +
                    "2. Find all palindromes\n\t" +
                    "3. Random word\n\t" +
                    "4. Find anagrams for a given word\n\t" +
                    "5. Find all anagrams\n\t" +
                    "0. Exit");
            String userMenuSelection = in.readLine();
            System.out.println("Read: " + userMenuSelection);


            if (userMenuSelection.equals("0")) {
                System.out.println("Exit");
                break;
            }

            DictionaryOperation operation = null;
            switch (userMenuSelection) {
                case "1":
                    operation = new SearchDictionaryOperation(wordsSet, in);
                    break;
                case "2":
                    operation = new PalindromeDictionaryOperation(wordsSet);
                    break;
                case "3":
                    operation = new RandomWordDictionaryOperation(wordsSet);
                    break;
                case "4":
                    operation = new AnagramDictionaryOperation(wordsSet, in);
                    break;
                case "5":
                    operation = new AnagramsListDictionaryOperation(wordsSet);
                    break;
                default:
                    System.out.println("Please select a valid option");
            }

            if (operation != null) operation.run();


        }

    }


}
