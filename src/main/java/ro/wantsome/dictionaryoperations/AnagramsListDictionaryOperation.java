package ro.wantsome.dictionaryoperations;

import ro.wantsome.Utils;

import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class AnagramsListDictionaryOperation implements DictionaryOperation {
    private Set<String> wordsSet;

    public AnagramsListDictionaryOperation(Set<String> wordsSet) {
        this.wordsSet = wordsSet;
    }

    @Override
    public void run() throws IOException {
        System.out.println("Find anagrams for all words");
        Map<String, List<String>> anagrams = Utils.findAnagramsForAllWords(wordsSet);
        for (Map.Entry<String, List<String>> entry : anagrams.entrySet()) {
            System.out.println("Key is: " + entry.getKey() + "  ,  Value is: " + entry.getValue());
        }
    }
}
