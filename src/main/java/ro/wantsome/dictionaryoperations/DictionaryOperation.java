package ro.wantsome.dictionaryoperations;

import java.io.IOException;

public interface DictionaryOperation {
    void run()throws IOException;
}
