package ro.wantsome.dictionaryoperations;

import ro.wantsome.Utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class AnagramDictionaryOperation implements DictionaryOperation {
    private BufferedReader in;
    private Set<String> wordsSet;

    public AnagramDictionaryOperation(Set<String> wordsSet, BufferedReader in) {
        this.in = in;
        this.wordsSet = wordsSet;
    }

    @Override
    public void run() throws IOException {
        System.out.println("Anagrams\n" + "Please enter a word: ");
        String userGivenWord = in.readLine();
        if ("".equals(userGivenWord))
            return;
        Map<String, List<String>> anagrams = Utils.findAnagramsForAGivenWord(wordsSet, userGivenWord);
        for (Map.Entry<String, List<String>> entry : anagrams.entrySet()) {
            System.out.println("Key is: " + entry.getKey() + "  ,  Value is: " + entry.getValue());
        }
    }
}
